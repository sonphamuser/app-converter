package activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import model.Frag_Luc;

import model.Frag_DoDai;
import com.son.simpleconverter.R;

import adapter.NavigationDrawerAdapter;
import model.FragKhoiLuong;
import model.Frag_NhietDo;
import model.Frag_ThoiGian;
import model.Frag_TocDo;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String[] nav_list_itemcontent = {"Khối lượng", "Nhiệt độ", "Thời gian", "Tốc độ", "Lực", "Độ dài"};
    int[] nav_list_itemimage ={R.drawable.khoiluong, R.drawable.nhietdo, R.drawable.thoigian, R.drawable.tocdo, R.drawable.luc,
    R.drawable.dodai};
    ListView nav_lv_items;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabCalculator = (FloatingActionButton) findViewById(R.id.fabCalculator);
        fabCalculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
/*                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/

                Intent intent = new Intent(MainActivity.this, ActionButton_Calculator.class);
                startActivity(intent);
            }
        });

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        nav_lv_items = (ListView) findViewById(R.id.nav_dra_list_content);
        NavigationDrawerAdapter adapter_items = new NavigationDrawerAdapter(this, nav_list_itemcontent, nav_list_itemimage);
        nav_lv_items.setAdapter(adapter_items);
        nav_lv_items.setItemChecked(0, true);
        getSupportActionBar().setTitle(nav_list_itemcontent[0]);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_fragment, new FragKhoiLuong()).commit();
        nav_lv_items.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        nav_lv_items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                nav_lv_items.setItemChecked(position, true);
                drawer.closeDrawers();
                getSupportActionBar().setTitle(nav_list_itemcontent[position]);

                if (position == 0) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_fragment, new FragKhoiLuong()).commit();
                } else if (position == 1) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_fragment, new Frag_NhietDo()).commit();
                } else if (position == 2) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_fragment, new Frag_ThoiGian()).commit();
                } else if (position == 3) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_fragment, new Frag_TocDo()).commit();
                } else if (position == 4) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_fragment, new Frag_Luc()).commit();
                } else if (position == 5) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_fragment, new Frag_DoDai()).commit();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
