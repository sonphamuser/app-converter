package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.son.simpleconverter.R;


/**
 * Created by Son on 11/12/2015.
 */
public class NavigationDrawerAdapter extends BaseAdapter{
    Context context;
    String[] content;
    int[] imagecontent;

    public NavigationDrawerAdapter(Context context, String[] content, int[] imagecontent)
    {
        this.context = context;
        this.content = content;
        this.imagecontent = imagecontent;
    }

    @Override
    public int getCount() {
        return content.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.nav_dra_item, null);

        ImageView imgview = (ImageView) view.findViewById(R.id.nav_imgContent);
        TextView txtview = (TextView) view.findViewById(R.id.nav_txtContent);

        txtview.setText(content[position]);
        imgview.setImageResource(imagecontent[position]);

        return view;
    }
}
