package model;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.son.simpleconverter.R;


public class Frag_DoDai extends android.support.v4.app.Fragment {
    TextView txtKm, txtMet, txtCm, txtMili, txtDam, txtYard, txtFoot, txtInch, txtDamHaiLy;
    EditText edtKm, edtMet, edtCm, edtMili,edtDam, edtYard, edtFoot, edtInch, edtDamHaiLy;
    public Frag_DoDai() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_do_dai, container, false);

        txtKm = (TextView) view.findViewById(R.id.fraDoDai_txtKm);
        txtMet = (TextView) view.findViewById(R.id.fraDoDai_txtMet);
        txtCm = (TextView) view.findViewById(R.id.fraDoDai_txtCm);
        txtMili = (TextView) view.findViewById(R.id.fraDoDai_txtMili);
        txtDam = (TextView) view.findViewById(R.id.fraDoDai_txtDam);
        txtYard = (TextView) view.findViewById(R.id.fraDoDai_txtYard);
        txtFoot = (TextView) view.findViewById(R.id.fraDoDai_txtFoot);
        txtInch = (TextView) view.findViewById(R.id.fraDoDai_txtInch);
        txtDamHaiLy = (TextView) view.findViewById(R.id.fraDoDai_txtDamHaiLy);

        edtKm = (EditText) view.findViewById(R.id.fraDoDai_edtKm);
        edtMet = (EditText) view.findViewById(R.id.fraDoDai_edtMet);
        edtCm = (EditText) view.findViewById(R.id.fraDoDai_edtCm);

        edtMili = (EditText) view.findViewById(R.id.fraDoDai_edtMili);
        edtDam = (EditText) view.findViewById(R.id.fraDoDai_edtDam);
        edtYard = (EditText) view.findViewById(R.id.fraDoDai_edtYard);

        edtFoot = (EditText) view.findViewById(R.id.fraDoDai_edtFoot);
        edtInch = (EditText) view.findViewById(R.id.fraDoDai_edtInch);
        edtDamHaiLy = (EditText) view.findViewById(R.id.fraDoDai_edtDamHaiLy);

        edtKm.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtKm.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtKm.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtKm.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtKm.isFocused() == true) {
                        double giatri = Double.valueOf(edtKm.getText().toString());
                        edtMet.setText("" +  (giatri * 1000));
                        edtCm.setText("" + ( giatri * 100000));
                        edtMili.setText("" + (giatri * 1000000));
                        edtDam.setText("" + (giatri * 0.6213712));
                        edtYard.setText("" + (giatri * 1093.613));
                        edtFoot.setText("" + (giatri * 3280.84));
                        edtInch.setText("" + (giatri * 39370.08));
                        edtDamHaiLy.setText("" + (giatri * 0.5399568));

                    }
                }
                else
                {
                    edtMet.setText("0");
                    edtCm.setText("0");
                    edtMili.setText("0");
                    edtDam.setText("0");
                    edtYard.setText("0");
                    edtFoot.setText("0" );
                    edtInch.setText("0" );
                    edtDamHaiLy.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtMet.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtMet.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtMet.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtMet.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtMet.isFocused() == true) {
                        double giatri = Double.valueOf(edtMet.getText().toString());
                        edtKm.setText("" +  (giatri * 0.001));
                        edtCm.setText("" + ( giatri * 100));
                        edtMili.setText("" + (giatri * 1000));
                        edtDam.setText("" + (giatri * 0.0006213712));
                        edtYard.setText("" + (giatri * 1.093613));
                        edtFoot.setText("" + (giatri * 3.28084));
                        edtInch.setText("" + (giatri * 39.37008));
                        edtDamHaiLy.setText("" + (giatri * 0.0005399568));

                    }
                }
                else
                {
                    edtKm.setText("0");
                    edtCm.setText("0");
                    edtMili.setText("0");
                    edtDam.setText("0");
                    edtYard.setText("0");
                    edtFoot.setText("0" );
                    edtInch.setText("0" );
                    edtDamHaiLy.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edtCm.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtCm.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtCm.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtCm.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtCm.isFocused() == true) {
                        double giatri = Double.valueOf(edtCm.getText().toString());
                        edtKm.setText("" +  (giatri * 0.00001));
                        edtMet.setText("" + ( giatri * 0.01));
                        edtMili.setText("" + (giatri * 10));
                        edtDam.setText("" + (giatri * 0.000006213712));
                        edtYard.setText("" + (giatri * 0.01093613));
                        edtFoot.setText("" + (giatri * 0.0328084));
                        edtInch.setText("" + (giatri * 0.3937008));
                        edtDamHaiLy.setText("" + (giatri * 0.000005399568));

                    }
                }
                else
                {
                    edtKm.setText("0");
                    edtMet.setText("0");
                    edtMili.setText("0");
                    edtDam.setText("0");
                    edtYard.setText("0");
                    edtFoot.setText("0" );
                    edtInch.setText("0" );
                    edtDamHaiLy.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtMili.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtMili.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtMili.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtMili.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtMili.isFocused() == true) {
                        double giatri = Double.valueOf(edtMili.getText().toString());
                        edtKm.setText("" +  (giatri * 0.000001));
                        edtMet.setText("" + ( giatri * 0.001));
                        edtCm.setText("" + (giatri * 0.1));
                        edtDam.setText("" + (giatri * 0.0000006213712));
                        edtYard.setText("" + (giatri * 0.001093613));
                        edtFoot.setText("" + (giatri * 0.00328084));
                        edtInch.setText("" + (giatri * 0.03937008));
                        edtDamHaiLy.setText("" + (giatri * 0.0000005399568));

                    }
                }
                else
                {
                    edtKm.setText("0");
                    edtMet.setText("0");
                    edtCm.setText("0");
                    edtDam.setText("0");
                    edtYard.setText("0");
                    edtFoot.setText("0" );
                    edtInch.setText("0" );
                    edtDamHaiLy.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edtDam.addTextChangedListener(new TextWatcher() {
            int lan = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtDam.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtDam.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtDam.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtDam.isFocused() == true) {
                        double giatri = Double.valueOf(edtDam.getText().toString());
                        edtKm.setText("" + (giatri * 1.609344));
                        edtMet.setText("" + (giatri * 1609.344));
                        edtCm.setText("" + (giatri * 160934.4));
                        edtMili.setText("" + (giatri * 1609344));
                        edtYard.setText("" + (giatri * 1760));
                        edtFoot.setText("" + (giatri * 5280));
                        edtInch.setText("" + (giatri * 63360));
                        edtDamHaiLy.setText("" + (giatri * 0.8689762));

                    }
                } else {
                    edtKm.setText("0");
                    edtMet.setText("0");
                    edtCm.setText("0");
                    edtMili.setText("0");
                    edtYard.setText("0");
                    edtFoot.setText("0");
                    edtInch.setText("0");
                    edtDamHaiLy.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtYard.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtYard.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtYard.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtYard.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtYard.isFocused() == true) {
                        double giatri = Double.valueOf(edtYard.getText().toString());
                        edtKm.setText("" + (giatri * 0.0009144));
                        edtMet.setText("" + ( giatri * 0.9144));
                        edtCm.setText("" + (giatri * 91.44));
                        edtMili.setText("" + (giatri * 914.4));
                        edtDam.setText("" + (giatri * 0.0005681818));
                        edtFoot.setText("" + (giatri * 3));
                        edtInch.setText("" + (giatri * 36));
                        edtDamHaiLy.setText("" + (giatri * 0.004937365));

                    }
                }
                else
                {
                    edtKm.setText("0");
                    edtMet.setText("0");
                    edtCm.setText("0");
                    edtMili.setText("0");
                    edtDam.setText("0");
                    edtFoot.setText("0" );
                    edtInch.setText("0" );
                    edtDamHaiLy.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtFoot.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtFoot.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtFoot.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtFoot.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtFoot.isFocused() == true) {
                        double giatri = Double.valueOf(edtFoot.getText().toString());
                        edtKm.setText("" + (giatri * 0.003048));
                        edtMet.setText("" + ( giatri * 0.3048));
                        edtCm.setText("" + (giatri * 30.48));
                        edtMili.setText("" + (giatri * 304.8));
                        edtDam.setText("" + (giatri * 0.0001893939));
                        edtYard.setText("" + (giatri * 0.3333333));
                        edtInch.setText("" + (giatri * 12));
                        edtDamHaiLy.setText("" + (giatri * 0.0001645788));

                    }
                }
                else
                {
                    edtKm.setText("0");
                    edtMet.setText("0");
                    edtCm.setText("0");
                    edtMili.setText("0");
                    edtDam.setText("0");
                    edtYard.setText("0");
                    edtInch.setText("0" );
                    edtDamHaiLy.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtInch.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtInch.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtInch.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtInch.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtInch.isFocused() == true) {
                        double giatri = Double.valueOf(edtInch.getText().toString());
                        edtKm.setText("" + (giatri * 0.0000254));
                        edtMet.setText("" + ( giatri * 0.0254));
                        edtCm.setText("" + (giatri * 2.54));
                        edtMili.setText("" + (giatri * 25.4));
                        edtDam.setText("" + (giatri * 0.00001578283));
                        edtYard.setText("" + (giatri * 0.02777778));
                        edtFoot.setText("" + (giatri * 0.08333333));
                        edtDamHaiLy.setText("" + (giatri * 0.000137149));
                    }
                }
                else
                {
                    edtKm.setText("0");
                    edtMet.setText("0");
                    edtCm.setText("0");
                    edtMili.setText("0");
                    edtDam.setText("0");
                    edtYard.setText("0");
                    edtFoot.setText("0" );
                    edtDamHaiLy.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtDamHaiLy.addTextChangedListener(new TextWatcher() {
            int lan = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtDamHaiLy.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtDamHaiLy.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtDamHaiLy.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtDamHaiLy.isFocused() == true) {
                        double giatri = Double.valueOf(edtDamHaiLy.getText().toString());
                        edtKm.setText("" + (giatri * 1.852));
                        edtMet.setText("" + (giatri * 1852));
                        edtCm.setText("" + (giatri * 185200));
                        edtMili.setText("" + (giatri * 1852000));
                        edtDam.setText("" + (giatri * 1.150779));
                        edtYard.setText("" + (giatri * 2025.372));
                        edtFoot.setText("" + (giatri * 6076.115));
                        edtInch.setText("" + (giatri * 72913.39));
                    }
                } else {
                    edtKm.setText("0");
                    edtMet.setText("0");
                    edtCm.setText("0");
                    edtMili.setText("0");
                    edtDam.setText("0");
                    edtYard.setText("0");
                    edtFoot.setText("0");
                    edtInch.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }


}
