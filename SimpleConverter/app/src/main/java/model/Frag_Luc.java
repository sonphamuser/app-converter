package model;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.son.simpleconverter.R;


public class Frag_Luc extends android.support.v4.app.Fragment {
    TextView txtNewton, txtOunce;
    EditText edtNewton, edtOunce;

    public Frag_Luc() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_luc, container, false);
        txtNewton = (TextView) view.findViewById(R.id.fraLuc_txtNewTon);
        txtOunce = (TextView) view.findViewById(R.id.fraLuc_txtOunce);

        edtNewton = (EditText) view.findViewById(R.id.fraLuc_edtNewTon);
        edtOunce = (EditText) view.findViewById(R.id.fraLuc_edtOunce);

        edtNewton.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtNewton.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtNewton.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtNewton.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtNewton.isFocused() == true) {
                        double giatri = Double.valueOf(edtNewton.getText().toString());
                        edtOunce.setText("" +  (giatri * 3.596943));

                    }
                }
                else
                {
                    edtOunce.setText("0");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edtOunce.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtOunce.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtOunce.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtOunce.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtOunce.isFocused() == true) {
                        double giatri = Double.valueOf(edtOunce.getText().toString());
                        edtNewton.setText("" +  (giatri * 0.2780139));

                    }
                }
                else
                {
                    edtNewton.setText("0");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }


}
