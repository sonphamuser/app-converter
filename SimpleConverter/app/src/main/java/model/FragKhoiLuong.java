package model;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.son.simpleconverter.R;

import java.math.BigDecimal;
import java.text.DecimalFormat;


public class FragKhoiLuong extends android.support.v4.app.Fragment {
    TextView txtTan, txtKg, txtGram;
    EditText edtTan, edtKg, edtGram;
    DecimalFormat f = new DecimalFormat("##.000000");
    public FragKhoiLuong() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_frag_khoi_luong, container, false);
        txtTan = (TextView) view.findViewById(R.id.fraKhoiLuong_txtTan);
        txtKg = (TextView) view.findViewById(R.id.fraKhoiLuong_txtKg);
        txtGram = (TextView) view.findViewById(R.id.fraKhoiLuong_txtGram);


        edtTan = (EditText) view.findViewById(R.id.fraKhoiLuong_edtTan);
        edtKg = (EditText) view.findViewById(R.id.fraKhoiLuong_edtKg);
        edtGram = (EditText) view.findViewById(R.id.fraKhoiLuong_edtGram);

        edtTan.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtTan.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtTan.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtTan.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtTan.isFocused() == true) {
                        double giatri = Double.valueOf(edtTan.getText().toString());
                        edtKg.setText("" +  (giatri * 1000));
                        edtGram.setText("" + ( giatri * 1000000));
                    }
                }
                else
                {
                    edtKg.setText("0");
                    edtGram.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtKg.addTextChangedListener(new TextWatcher() {

            int lan = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtKg.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtKg.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtKg.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtKg.isFocused() == true) {
                        double giatri = Double.valueOf(edtKg.getText().toString());
                        edtTan.setText("" + (giatri * 0.001));
                        edtGram.setText("" + (( giatri * 1000)));
                    }
                }
                else
                {
                    edtTan.setText("0");
                    edtGram.setText("0");
                }

            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtGram.addTextChangedListener(new TextWatcher() {

            int lan = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    txtGram.setVisibility(View.GONE);
                    lan = 0;

                } else {
                    if (lan == 0) {

                        txtGram.setVisibility(View.VISIBLE);
                        Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                        txtGram.startAnimation(ani);
                        lan = 1;


                    }
                }
                if (s.length() > 0) {
                    if (edtGram.isFocused() == true) {
                        double giatri = Double.valueOf(edtGram.getText().toString());
                        edtTan.setText("" + (giatri * 0.000001));
                        edtKg.setText("" + ( giatri * 0.001));
                    }
                }
                else
                {
                    edtTan.setText("0");
                    edtKg.setText("0");
                }

            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }


}
