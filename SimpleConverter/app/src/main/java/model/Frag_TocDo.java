package model;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.son.simpleconverter.R;


public class Frag_TocDo extends android.support.v4.app.Fragment {
    TextView txtDam, txtFeet, txtMet, txtKm, txtHaili;
    EditText edtDam, edtFeet, edtMet, edtKm, edtHaili;

    public Frag_TocDo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_toc_do,container,false);
        txtDam = (TextView) view.findViewById(R.id.fraTocDo_txtDamGio);
        txtFeet = (TextView) view.findViewById(R.id.fraTocDo_txtFeetGiay);
        txtMet = (TextView) view.findViewById(R.id.fraTocDo_txtMetGiay);
        txtKm = (TextView) view.findViewById(R.id.fraTocDo_txtKmGio);
        txtHaili = (TextView) view.findViewById(R.id.fraTocDo_txtHailiGio);

        edtDam = (EditText) view.findViewById(R.id.fraTocDo_edtDamGio);
        edtFeet = (EditText) view.findViewById(R.id.fraTocDo_edtFeetGiay);
        edtMet = (EditText) view.findViewById(R.id.fraTocDo_edtMetGiay);
        edtKm = (EditText) view.findViewById(R.id.fraTocDo_edtKmGio);
        edtHaili = (EditText) view.findViewById(R.id.fraTocDo_edtHailiGio);

        edtDam.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtDam.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtDam.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtDam.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtDam.isFocused() == true) {
                        double giatri = Double.valueOf(edtDam.getText().toString());
                        edtFeet.setText("" + (giatri * 1.466667));
                        edtMet.setText("" +  (giatri * 0.44704));
                        edtKm.setText("" + (giatri * 1.609344));
                        edtHaili.setText("" + (giatri * 0.8689765));
                    }
                }
                else
                {
                    edtFeet.setText("0");
                    edtMet.setText("0");
                    edtKm.setText("0");
                    edtHaili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtFeet.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtFeet.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtFeet.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtFeet.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtFeet.isFocused() == true) {
                        double giatri = Double.valueOf(edtFeet.getText().toString());
                        edtDam.setText("" + (giatri * 0.681818));
                        edtMet.setText("" +  (giatri * 0.3048));
                        edtKm.setText("" + (giatri * 1.09728));
                        edtHaili.setText("" + (giatri * 0.5924838));
                    }
                }
                else
                {
                    edtDam.setText("0");
                    edtMet.setText("0");
                    edtKm.setText("0");
                    edtHaili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtMet.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtMet.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtMet.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtMet.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtMet.isFocused() == true) {
                        double giatri = Double.valueOf(edtMet.getText().toString());
                        edtDam.setText("" + (giatri * 2.236936));
                        edtFeet.setText("" +  (giatri *3.28084));
                        edtKm.setText("" + (giatri * 3.6));
                        edtHaili.setText("" + (giatri * 1.943844));
                    }
                }
                else
                {
                    edtDam.setText("0");
                    edtFeet.setText("0");
                    edtKm.setText("0");
                    edtHaili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtKm.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtKm.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtKm.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtKm.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtKm.isFocused() == true) {
                        double giatri = Double.valueOf(edtKm.getText().toString());
                        edtDam.setText("" + (giatri * 0.6213712));
                        edtFeet.setText("" +  (giatri * 0.9113444));
                        edtMet.setText("" + (giatri * 0.2777778));
                        edtHaili.setText("" + (giatri * 0.5399568));
                    }
                }
                else
                {
                    edtDam.setText("0");
                    edtFeet.setText("0");
                    edtMet.setText("0");
                    edtHaili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtHaili.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtHaili.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtHaili.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtHaili.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtHaili.isFocused() == true) {
                        double giatri = Double.valueOf(edtHaili.getText().toString());
                        edtDam.setText("" + (giatri * 1.150779));
                        edtFeet.setText("" +  (giatri * 1.68781));
                        edtMet.setText("" + (giatri / 0.5144444));
                        edtKm.setText("" + (giatri * 1.852));
                    }
                }
                else
                {
                    edtDam.setText("0");
                    edtFeet.setText("0");
                    edtMet.setText("0");
                    edtKm.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

}
