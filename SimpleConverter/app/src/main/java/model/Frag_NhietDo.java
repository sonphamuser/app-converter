package model;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.son.simpleconverter.R;

import java.math.BigDecimal;
import java.text.DecimalFormat;


public class Frag_NhietDo extends android.support.v4.app.Fragment {
    TextView txtF, txtC, txtKel;
    EditText edtF, edtC, edtKel;
    public Frag_NhietDo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_frag_nhiet_do, container, false);
        txtF = (TextView) view.findViewById(R.id.fraNhietDo_txtFahrenheit);
        txtC = (TextView) view.findViewById(R.id.fraNhietDo_txtCelsius);
        txtKel = (TextView) view.findViewById(R.id.fraNhietDo_txtKelvin);


        edtF = (EditText) view.findViewById(R.id.fraNhietDo_edtFahrenheit);
        edtC = (EditText) view.findViewById(R.id.fraNhietDo_edtCelsius);
        edtKel = (EditText) view.findViewById(R.id.fraNhietDo_edtKelvin);

        edtC.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtC.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtC.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtC.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtC.isFocused() == true) {
                        double giatri = Double.valueOf(edtC.getText().toString());
                        edtF.setText("" + ((giatri * 1.8) + 32));
                        edtKel.setText("" +  (giatri + 273.15));
                    }
                }
                else
                {
                    edtF.setText("0");
                    edtKel.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtF.addTextChangedListener(new TextWatcher() {
            DecimalFormat f = new DecimalFormat("##.00000");
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtF.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtF.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtF.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtF.isFocused() == true) {
                        double giatri = Double.valueOf(edtF.getText().toString());
                        edtC.setText("" + f.format((giatri - 32) / 1.8));
                        edtKel.setText("" +  f.format((giatri + 459.67) * 5/9));
                    }
                }
                else
                {
                    edtC.setText("0");
                    edtKel.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edtKel.addTextChangedListener(new TextWatcher() {
            DecimalFormat f = new DecimalFormat("##.00");
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtKel.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtKel.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtKel.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtKel.isFocused() == true) {
                        double giatri = Double.valueOf(edtKel.getText().toString());
                        edtC.setText("" + f.format((giatri - 273.15)));
                        edtF.setText("" +  f.format((giatri - 273.15) * 1.8 + 32));
                    }
                }
                else
                {
                    edtC.setText("0");
                    edtF.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }
}

