package model;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.son.simpleconverter.R;

import java.math.BigDecimal;


public class Frag_ThoiGian extends android.support.v4.app.Fragment {
    TextView txtMili, txtGiay, txtPhut, txtGio, txtNgay, txtTuan;
    EditText edtMili, edtGiay, edtPhut, edtGio, edtNgay, edtTuan;

    public Frag_ThoiGian() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_frag_thoi_gian, container, false);

        txtMili = (TextView) view.findViewById(R.id.fraThoiGian_txtMiliGiay);
        txtGiay = (TextView) view.findViewById(R.id.fraThoiGian_txtGiay);
        txtPhut = (TextView) view.findViewById(R.id.fraThoiGian_txtPhut);
        txtGio = (TextView) view.findViewById(R.id.fraThoiGian_txtGio);
        txtNgay = (TextView) view.findViewById(R.id.fraThoiGian_txtNgay);
        txtTuan = (TextView) view.findViewById(R.id.fraThoiGian_txtTuan);

        edtMili = (EditText) view.findViewById(R.id.fraThoiGian_edtMiliGiay);
        edtGiay = (EditText) view.findViewById(R.id.fraThoiGian_edtGiay);
        edtPhut = (EditText) view.findViewById(R.id.fraThoiGian_edtPhut);
        edtGio = (EditText) view.findViewById(R.id.fraThoiGian_edtGio);
        edtNgay = (EditText) view.findViewById(R.id.fraThoiGian_edtNgay);
        edtTuan = (EditText) view.findViewById(R.id.fraThoiGian_edtTuan);

        edtMili.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtMili.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtMili.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtMili.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtMili.isFocused() == true) {
                        double giatri = Double.valueOf(edtMili.getText().toString());
                        edtTuan.setText("" + (giatri / 604800000));
                        edtNgay.setText("" +  (giatri / 86400000));
                        edtGio.setText("" + (giatri / 3600000));
                        edtPhut.setText("" + (giatri / 60000));
                        edtGiay.setText("" + (giatri / 1000));
                    }
                }
                else
                {
                    edtTuan.setText("0");
                    edtNgay.setText("0");
                    edtGio.setText("0");
                    edtPhut.setText("0");
                    edtGiay.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtGiay.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtGiay.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtGiay.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtGiay.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtGiay.isFocused() == true) {
                        double giatri = Double.valueOf(edtGiay.getText().toString());
                        edtTuan.setText("" + (giatri / 604800));
                        edtNgay.setText("" +  (giatri / 86400));
                        edtGio.setText("" + (giatri / 3600));
                        edtPhut.setText("" + (giatri / 60));
                        edtMili.setText("" + (giatri * 1000));
                    }
                }
                else
                {
                    edtTuan.setText("0");
                    edtNgay.setText("0");
                    edtGio.setText("0");
                    edtPhut.setText("0");
                    edtMili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtPhut.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtPhut.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtPhut.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtPhut.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtPhut.isFocused() == true) {
                        double giatri = Double.valueOf(edtPhut.getText().toString());
                        edtTuan.setText("" + (giatri / 10080));
                        edtNgay.setText("" +  (giatri / 1440));
                        edtGio.setText("" + (giatri / 60));
                        edtGiay.setText("" + (giatri * 60));
                        edtMili.setText("" + (giatri * 60000));
                    }
                }
                else
                {
                    edtTuan.setText("0");
                    edtNgay.setText("0");
                    edtGio.setText("0");
                    edtGiay.setText("0");
                    edtMili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtGio.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtGio.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtGio.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtGio.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtGio.isFocused() == true) {
                        double giatri = Double.valueOf(edtGio.getText().toString());
                        edtTuan.setText("" + (giatri / 168));
                        edtNgay.setText("" +  (giatri / 24));
                        edtPhut.setText("" + (giatri * 60));
                        edtGiay.setText("" + (giatri * 3600));
                        edtMili.setText("" + (giatri * 3600000));
                    }
                }
                else
                {
                    edtTuan.setText("0");
                    edtNgay.setText("0");
                    edtPhut.setText("0");
                    edtGiay.setText("0");
                    edtMili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtNgay.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtNgay.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtNgay.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtNgay.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtNgay.isFocused() == true) {
                        double giatri = Double.valueOf(edtNgay.getText().toString());
                        edtTuan.setText("" + (giatri / 7));
                        edtGio.setText("" +  (giatri * 24));
                        edtPhut.setText("" + (giatri * 1440));
                        edtGiay.setText("" + (giatri * 86400));
                        edtMili.setText("" + (giatri * 86400000));
                    }
                }
                else
                {
                    edtTuan.setText("0");
                    edtGio.setText("0");
                    edtPhut.setText("0");
                    edtGiay.setText("0");
                    edtMili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtTuan.addTextChangedListener(new TextWatcher() {
            int lan = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() <=0)
                {
                    txtTuan.setVisibility(View.GONE);
                    lan = 0;
                }
                else if (lan == 0)
                {
                    txtTuan.setVisibility(View.VISIBLE);
                    Animation ani = AnimationUtils.loadAnimation(getActivity(), R.anim.ani);
                    txtTuan.startAnimation(ani);
                    lan = 1;
                }
                if(s.length() > 0)
                {
                    if(edtTuan.isFocused() == true) {
                        double giatri = Double.valueOf(edtTuan.getText().toString());
                        edtNgay.setText("" + (giatri * 7));
                        edtGio.setText("" +  (giatri * 168));
                        edtPhut.setText("" + (giatri * 10080));
                        edtGiay.setText("" + (giatri * 604800));
                        edtMili.setText("" + (giatri * 604800000));
                    }
                }
                else
                {
                    edtNgay.setText("0");
                    edtGio.setText("0");
                    edtPhut.setText("0");
                    edtGiay.setText("0");
                    edtMili.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

}
